# Practicas RAID
## Pràctica (1):  Treball bàsic amb raid
Instalar el paquete de ordenes para RAID
~$ sudo apt install 

### Crear el RAID
- Crear 3 unidades fisicas "imaginarias" usando ***dd***.
- Asignar un disp fisico de loopback.
```
~$ sudo dd if=/dev/zero of=disk01.img bs=1k count=100K
~$ sudo dd if=/dev/zero of=disk02.img bs=1k count=100K
~$ sudo dd if=/dev/zero of=disk03.img bs=1k count=100K

~$ sudo losetup /dev/loop0 disk01.img
~$ sudo losetup /dev/loop1 disk02.img
~$ sudo losetup /dev/loop2 disk03.img
~$ sudo losetup -a  # chequa que los disco esten asignados a un dispositivo
```
- Integrarlos a una RAID formado por los 3 disp.
```
mdadm --create <nom_Disp_RAID> --chunk=<n_fragmentos> --level=<levelRAID> --raid-devices=<> <dispositvos>
```
```
~$ sudo mdadm --create /dev/md0 --chunk=4 --level=1 --raid-devices=3 /dev/loop0 /dev/loop1 /dev/loop2
mdadm: Note: this array has metadata at the start and
    may not be suitable as a boot device.  If you plan to
    store '/boot' on this device please ensure that
    your boot-loader understands md/v1.x metadata, or use
    --metadata=0.90
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.
~$ tree /dev/disk
/dev/disk
├── by-id
│   ├── md-name-i10:0 -> ../../md0
│   ├── md-uuid-acff7ad3:4eac00bb:028cd7f8:6349cee3 -> ../../md0
...
```
Ahora el sistema tiene un nuevo disp (/dev/md0). Un disco RAID formado por las 3 particiones loops.
Se trata de una raid level 1 con 3 mirror disks, pero el sistema ve un solo disp de 100M.
Hace falta asignarle un filesystem y montarlo para poder utilizarlo
```
~$ sudo mkfs -t ext4 /dev/md0
mke2fs 1.46.2 (28-Feb-2021)
Discarding device blocks: done                            
Creating filesystem with 101376 1k blocks and 25376 inodes
Filesystem UUID: 08e06dc2-94b5-4029-a4cf-87715644af3c
Superblock backups stored on blocks: 
	8193, 24577, 40961, 57345, 73729

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (4096 blocks): done
Writing superblocks and filesystem accounting information: done 

~$ sudo mount /dev/md0 /mnt
~$ ls -l /mnt

~$ sudo cp -r /boot/ /mnt/md0

~$ df -h

```