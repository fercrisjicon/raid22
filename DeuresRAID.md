## Pràctica (1) RAID+LVM  en loops:
### - Raid 1 dos dics de 500M.
- crear los discos necesarios
    ```
    # dd if=/dev/zero of=disk01.img bs=1k count=500k
    512000+0 records in
    512000+0 records out
    524288000 bytes (524 MB, 500 MiB) copied, 0.58894 s, 890 MB/s

    # dd if=/dev/zero of=disk02.img bs=1k count=500k
    512000+0 records in
    512000+0 records out
    524288000 bytes (524 MB, 500 MiB) copied, 0.591042 s, 887 MB/s

    # dd if=/dev/zero of=disk03.img bs=1k count=500k
    512000+0 records in
    512000+0 records out
    524288000 bytes (524 MB, 500 MiB) copied, 0.542048 s, 967 MB/s

    # dd if=/dev/zero of=disk04.img bs=1k count=500k
    512000+0 records in
    512000+0 records out
    524288000 bytes (524 MB, 500 MiB) copied, 0.579557 s, 905 MB/s
    ```
- asignarlos a un dispositivo
    ```
    # losetup /dev/loop1 disk01.img
    # losetup /dev/loop2 disk02.img
    # losetup /dev/loop3 disk03.img
    # losetup /dev/loop4 disk04.img
    # losetup -a
    /dev/loop1: [66306]:3675960 (/var/tmp/m11/raid22/disk01.img)
    /dev/loop4: [66306]:3676264 (/var/tmp/m11/raid22/disk04.img)
    /dev/loop2: [66306]:3676262 (/var/tmp/m11/raid22/disk02.img)
    /dev/loop3: [66306]:3676263 (/var/tmp/m11/raid22/disk03.img)
    ```
- crear la raid de level 1 con los dos disco
    ```
    # mdadm --create /dev/md/raid_lvm1 --level=1 --raid-devices=2 /dev/loop1 /dev/loop2
    mdadm: Note: this array has metadata at the start and
        may not be suitable as a boot device.  If you plan to
        store '/boot' on this device please ensure that
        your boot-loader understands md/v1.x metadata, or use
        --metadata=0.90
    Continue creating array? yes
    mdadm: Defaulting to version 1.2 metadata
    mdadm: array /dev/md/raid_lvm2 started.
    ```
    ***Podemos comprovar que esta bien creada con:***
    ```
    # cat /porc/mdstat
    Personalities : [linear] [multipath] [raid0] [raid1] [raid6] [raid5] [raid4] [raid10] 
    md126 : active raid1 loop2[1] loop1[0]
        510976 blocks super 1.2 [2/2] [UU]
        
    unused devices: <none>

    # mdadm --detail /dev/md/raid_lvm1
    /dev/md/raid_lvm1:
           Version : 1.2
     Creation Time : Fri Feb 18 10:36:32 2022
        Raid Level : raid1
        Array Size : 510976 (499.00 MiB 523.24 MB)
     Used Dev Size : 510976 (499.00 MiB 523.24 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Fri Feb 18 10:36:34 2022
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

    Consistency Policy : resync

              Name : i10:raid_lvm1  (local to host i10)
              UUID : b6af60ee:ad20684f:536bbfd1:301ffc53
            Events : 18

    Number   Major   Minor   RaidDevice State
       0       7        1        0      active sync   /dev/loop1
       1       7        2        1      active sync   /dev/loop2
    ```

### - Un segon Raid1 (dos discs de 500M).
- crearmos el segundo raid (igual que el anterior)
    ```
    # mdadm --create /dev/md/raid_lvm2 --level=1 --raid-devices=2 /dev/loop3 /dev/loop4
    # cat /proc/mdstat
    Personalities : [linear] [multipath] [raid0] [raid1] [raid6] [raid5] [raid4] [raid10] 
    md127 : active raid1 loop4[1] loop3[0]
        510976 blocks super 1.2 [2/2] [UU]
        
    md126 : active raid1 loop2[1] loop1[0]
        510976 blocks super 1.2 [2/2] [UU]
        
    unused devices: <none>

    # mdadm --detail /dev/md/raid_lvm2
    /dev/md/raid_lvm2:
           Version : 1.2
     Creation Time : Fri Feb 18 10:36:11 2022
        Raid Level : raid1
        Array Size : 510976 (499.00 MiB 523.24 MB)
     Used Dev Size : 510976 (499.00 MiB 523.24 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Fri Feb 18 10:36:13 2022
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

    Consistency Policy : resync

              Name : i10:raid_lvm2  (local to host i10)
              UUID : 8b79f843:2377b27c:99ea3556:c277126d
            Events : 17

    Number   Major   Minor   RaidDevice State
       0       7        3        0      active sync   /dev/loop3
       1       7        4        1      active sync   /dev/loop4
    ```
    ***Podemos ver que estan bien***
    ```
    # tree /dev/disk
    │   ├── md-name-i10:raid_lvm1 -> ../../md126
    │   ├── md-name-i10:raid_lvm2 -> ../../md127
    │   ├── md-uuid-8b79f843:2377b27c:99ea3556:c277126d -> ../../md127
    │   ├── md-uuid-b6af60ee:ad20684f:536bbfd1:301ffc53 -> ../../md126
    ```
### - Primer raid crear PV i VG mydisc.
- convertir en PV la primera raid
    ```
    # pvcreate /dev/md/raid_lvm1
    Physical volume "/dev/md/raid_lvm1" successfully created.

    # pvdisplay /dev/md/raid_lvm1  
    "/dev/md126" is a new physical volume of "499.00 MiB"
    --- NEW Physical volume ---
    PV Name               /dev/md126
    VG Name               
    PV Size               499.00 MiB
    Allocatable           NO
    PE Size               0   
    Total PE              0
    Free PE               0
    Allocated PE          0
    PV UUID               ZWXzz1-6pjW-8prJ-11RG-8d6u-8xVU-5rg2lH
    ```
- luego crear el VG con el PV de la primera raid
    ```
    # vgcreate mydisc /dev/md/raid_lvm1
    Volume group "mydisc" successfully created
    # vgdisplay mydisc
    --- Volume group ---
    VG Name               mydisc
    System ID             
    Format                lvm2
    Metadata Areas        1
    Metadata Sequence No  1
    VG Access             read/write
    VG Status             resizable
    MAX LV                0
    Cur LV                0
    Open LV               0
    Max PV                0
    Cur PV                1
    Act PV                1
    VG Size               496.00 MiB
    PE Size               4.00 MiB
    Total PE              124
    Alloc PE / Size       0 / 0   
    Free  PE / Size       124 / 496.00 MiB
    VG UUID               j2ED7R-c90U-fdpf-NjoI-1ecL-RcgL-OdIgl6
    ```

### - LV de 200M sistema, LV de 100M dades.
- crear los VL de sistema(200M) y dades(100M)
    ```
    # lvcreate -L 200M -n sistema /dev/mydisc
    Logical volume "sistema" created.
    # lvcreate -L 100M -n dades /dev/mydisc
    Logical volume "dades" created.

    # lvdisplay /dev/mydisc/sistema
    --- Logical volume ---
    LV Path                /dev/mydisc/sistema
    LV Name                sistema
    VG Name                mydisc
    LV UUID                SgaoIl-1rdl-ZUkA-5Md6-GJ6d-4a6c-L8qyfW
    LV Write Access        read/write
    LV Creation host, time i10, 2022-02-18 11:32:52 +0100
    LV Status              available
    # open                 0
    LV Size                200.00 MiB
    Current LE             50
    Segments               1
    Allocation             inherit
    Read ahead sectors     auto
    - currently set to     256
    Block device           253:0
    # lvdisplay /dev/mydisc/dades
    --- Logical volume ---
    LV Path                /dev/mydisc/dades
    LV Name                dades
    VG Name                mydisc
    LV UUID                CAnTTp-8oFQ-rqXO-s9vi-tp9O-MXol-FmLGdC
    LV Write Access        read/write
    LV Creation host, time i10, 2022-02-18 11:33:09 +0100
    LV Status              available
    # open                 0
    LV Size                100.00 MiB
    Current LE             25
    Segments               1
    Allocation             inherit
    Read ahead sectors     auto
    - currently set to     256
    Block device           253:1
    ```

### - Mkfs i Muntar i posar-hi dades
- montar un sistema de ficheros a los dos LV
    ```
    # mkfs -t ext4 /dev/mydisc/sistema
    mke2fs 1.46.2 (28-Feb-2021)
    Discarding device blocks: done                            
    Creating filesystem with 204800 1k blocks and 51200 inodes
    Filesystem UUID: 067c69ed-0710-47b3-acfa-8665d04c75af
    Superblock backups stored on blocks: 
        8193, 24577, 40961, 57345, 73729

    Allocating group tables: done                            
    Writing inode tables: done                            
    Creating journal (4096 blocks): done
    Writing superblocks and filesystem accounting information: done 

    # mkfs -t ext4 /dev/mydisc/dades
    mke2fs 1.46.2 (28-Feb-2021)
    Discarding device blocks: done                            
    Creating filesystem with 102400 1k blocks and 25688 inodes
    Filesystem UUID: 9be7c11d-7655-4e6c-a4bd-d60a173273cc
    Superblock backups stored on blocks: 
        8193, 24577, 40961, 57345, 73729

    Allocating group tables: done                            
    Writing inode tables: done                            
    Creating journal (4096 blocks): done
    Writing superblocks and filesystem accounting information: done 

    ```
- montarlas dentro de mnt, antes crear directorios
    ```
    # mkdir /mnt/sistema
    # mkdir /mnt/dades
    # mount /dev/mydisc/sistema /mnt/sistema
    # mount /dev/mydisc/dades /mnt/dades
    # mount
    /dev/mapper/mydisc-sistema on /mnt/sistema type ext4 (rw,relatime)
    /dev/mapper/mydisc-dades on /mnt/dades type ext4 (rw,relatime)
    ```
- copiar xixa dentro de sistema y dades
    ```
    # cp -r /bin/x* /mnt/sistema/.
    # cp -r /bin/a* /mnt/dades/.
    # cp -r /bin/b* /mnt/dades/.
    # df -h
    /dev/mapper/mydisc-sistema  189M   41M  134M  24% /mnt/sistema
    /dev/mapper/mydisc-dades     92M   12M   73M  15% /mnt/dades
    ```

### - Incorporar al VG el segon raid.
- extender el VG con la segunda raid
    ```
    # vgextend mydisc /dev/md/raid_lvm2
    Physical volume "/dev/md/raid_lvm2" successfully created.
    Volume group "mydisc" successfully extended
    # vgdisplay mydisc
    --- Volume group ---
    VG Name               mydisc
    System ID             
    Format                lvm2
    Metadata Areas        2
    Metadata Sequence No  4
    VG Access             read/write
    VG Status             resizable
    MAX LV                0
    Cur LV                2
    Open LV               2
    Max PV                0
    Cur PV                2
    Act PV                2
    VG Size               992.00 MiB
    PE Size               4.00 MiB
    Total PE              248
    Alloc PE / Size       75 / 300.00 MiB
    Free  PE / Size       173 / 692.00 MiB
    VG UUID               j2ED7R-c90U-fdpf-NjoI-1ecL-RcgL-OdIgl6
    ```
### - Incrementar sistema +100M.
- incrementar 100M a sistema
    ```
    # lvextend -L +100M /dev/mydisc/sistema
    Size of logical volume mydisc/sistema changed from 200.00 MiB (50 extents) to 300.00 MiB (75 extents).
    Logical volume mydisc/sistema successfully resized.
    # lvdisplay /dev/mydisc/sistema
    --- Logical volume ---
    LV Path                /dev/mydisc/sistema
    LV Name                sistema
    VG Name                mydisc
    LV UUID                SgaoIl-1rdl-ZUkA-5Md6-GJ6d-4a6c-L8qyfW
    LV Write Access        read/write
    LV Creation host, time i10, 2022-02-18 11:32:52 +0100
    LV Status              available
    # open                 1
    LV Size                300.00 MiB
    Current LE             75
    Segments               2
    Allocation             inherit
    Read ahead sectors     auto
    - currently set to     256
    Block device           253:0
    ```
- aumentos el sistema de ficheros de sistema
    ```
    # df -h
    /dev/mapper/mydisc-sistema  189M   41M  134M  24% /mnt/sistema
    
    # resize2fs /dev/mydisc/sistema
    resize2fs 1.46.2 (28-Feb-2021)
    Filesystem at /dev/mydisc/sistema is mounted on /mnt/sistema; on-line resizing required
    old_desc_blocks = 2, new_desc_blocks = 3
    The filesystem on /dev/mydisc/sistema is now 307200 (1k) blocks long.
    
    # df -h
    /dev/mapper/mydisc-sistema  287M   43M  226M  16% /mnt/sistema
    ```
### - Provocar un fail a un dels disc del raid1
    ```
    # mdadm /dev/md/raid_lvm1 --fail /dev/loop1
    ```
### - Eliminar completament el raid1 del VG.
    ```
    # vgremove mydisc /dev/md/raid_lvm1
    ```

## Pràctica (2) REAL
- Crear sda2: 10G, sda3: 4G, sda4: 4G
- Crear un raid1 amb sda3 i sda4
- generar un VG anomenat diskedt amb sda3 i sda4
- generar lv sistema de 3G, Dades de 1G
- muntar i posar-hi xixa.
- automatitzar el muntatge (/mnt/dades, /mnt/sistema)
- reboot i verificar que dades i sistema estan disponibles.