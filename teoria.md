#### APUNTES DE RAID:
# RAID - Redundant Array of Inexpensive Disk
La idea basica del RAID es combinar varias unidades de discos pequeños y economicas en una array para lograr objetivos de rendimiento que no se puede lograr con una una unidad mas grande y costosa.
Esta array, a ojos de la computadora, parece una unidad de almacenamiento.
RAID distribuye la informacion en varios discos.
Utiliza tecnicas como:
- RAID Level0: fragmentacion de discos(partir)
- RAID Level1: duplicacion de discos(copia)
- RAID Level5: fragmentacion de disco con paridad

para conseguir:
- redundancia
- menor latencia
- mayor ancho de banda
- capacidad max para recuperarse de fallas en el disco duro

RAID distribuye los datos en cada unidad de la array dividiendolos en fragmentos (256k/512k/otros valores).
Luego los fragmentos se escriben en el disco duro de la array, segun el Level de la RAID.
Cuando se leen los datos, el proceso se invierte, haciendo que las múltiples unidades de la array son en realidad una unidad grande.

### Firmware RAID
Tambien conocido como ATARAID.
Es un tipo de software donde los conjuntos de RAID pueden configurarse en un menu basado en firmware.


### Hardware RAID

### Software RAID

## Levels RAID
## RAID 0

## RAID 1

## RAID 5